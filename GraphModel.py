import matplotlib.pyplot as plt
import numpy as np


class GraphModel():
    def __init__(self, group, list_data, list_label, x_label='', y_label='', tick_label=[]):
        self._index = group
        self._data = list_data
        self._label = list_label
        self._x_label = x_label
        self._y_label = y_label
        self._tick = tick_label

    def show_plot(self):
        fig, ax = plt.subplots()
        index = np.arange(self._index)
        bar_width = 0.25
        for idx, elem in enumerate(self._data):
            ax.bar(index + bar_width * idx, elem, bar_width, label=self._label[idx])
        ax.set_xlabel(self._x_label)
        ax.set_ylabel(self._y_label)
        ax.set_xticks(index + bar_width)
        ax.set_xticklabels(self._tick)
        ax.legend(loc=4)  # put in lower right
        plt.savefig('results/%s.png' % self._x_label)
        fig.tight_layout()
        plt.show()

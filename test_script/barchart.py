import numpy as np
import matplotlib.pyplot as plt
from GraphModel import GraphModel as GModel


n_groups = 5

means_a = (20, 35, 30, 35, 27)
means_b = (25, 32, 34, 20, 25)
means_c = (27, 17, 33, 30, 40)

g_model = GModel(n_groups, [means_a, means_b, means_c],
                 ['A', 'B', 'C'], 'Group', 'Scores',
                 ['A', 'B', 'C', 'D', 'E'])
# g_model = GModel(n_groups, [means_a, means_b],
#                  ['A', 'B'], 'Group', 'Scores',
#                  ['A', 'B', 'C', 'D', 'E'])
g_model.show_plot()

"""
TODO:
1. create plots (convergence, fms, vms) for repetitions from 1 -> 4 -> 8 -> 10
2. create plots (convergence, fms, vms) for tolerance from 0.0001 -> 0.001 -> 0.01 -> 0.1
"""

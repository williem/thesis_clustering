# libraries import
import time
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import datasets


# load iris dataset
iris = datasets.load_iris()

# the iris contains 4 attributes: sepal length, sepal width, petal length, petal width
# get all rows of the first two columns of feature for the data and all the existing targets
data = iris.data[:, :2]
# contains 3 targets: setosa, versicolor, and virginica
target = iris.target

start_time = time.perf_counter()
# apply k-means clustering with random method, n_init=1 and max_iter=10 with randomized state
kmeans = KMeans(n_clusters=3, init="random", n_init=1, max_iter=10, algorithm='full', tol=1e-3,
                random_state=np.random.RandomState().randint(0, 1000), verbose=False)
kmeans.fit(data)
print("Converges in %f seconds" % (time.perf_counter() - start_time))

labels = kmeans.labels_
centers = kmeans.cluster_centers_

# take all rows from the third and fourth column, plot and show using scatterplot with centroids displayed
plt.xlabel(iris.feature_names[0])
plt.ylabel(iris.feature_names[1])
plt.scatter(data[:, 0], data[:, 1], c=labels, cmap='rainbow')
plt.scatter(centers[:, 0], centers[:, 1], c='black', marker='x')
plt.show()

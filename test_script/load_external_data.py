import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.datasets import fetch_covtype, load_files
from sklearn.metrics import fowlkes_mallows_score as fms
from sklearn.metrics.cluster import v_measure_score as vms

dataset = pd.read_csv('../data/ecoli.csv', header=None)
print(dataset)
# a, b = 1, 5
a, b = 1, 6
# a, b = 1, 7
# a, b = 2, 6
# a, b = 2, 7
# a, b = 5, 6
# a, b = 5, 7
data = dataset.loc[:, [a, b]]
# data = dataset.loc[:, [1, 2, 3, 4, 5, 6, 7]]
target_dataset = dataset[8].tolist()
tbl, target = np.unique(target_dataset, return_inverse=True)

start_time = time.perf_counter()
# # apply k-means clustering with random method, n_init=1 and max_iter=10 with randomized state
kmeans = KMeans(n_clusters=8, init="k-means++", n_init=1, max_iter=10, algorithm='full', tol=1e-4,
                random_state=np.random.RandomState().randint(0, 1000), verbose=True)
kmeans.fit(data)
print("Converges in %f seconds" % (time.perf_counter() - start_time))

labels = kmeans.labels_
centers = kmeans.cluster_centers_
# print(labels)
print("FMS", round(fms(target, labels) * 100, 4))
print("VMS", round(vms(target, labels) * 100, 4))

# take all rows from the third and fourth column, plot and show using scatterplot with centroids displayed
# plt.scatter(data[a], data[b], c=labels, cmap='rainbow')
# plt.scatter(centers[:, 0], centers[:, 1], c='black', marker='x')
# plt.show()

# libraries import
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import datasets


# load iris dataset
iris = datasets.load_iris()

# the iris contains 4 attributes: sepal length, sepal width, petal length, petal width
# get all rows of the first two columns of feature for the data and all the existing targets
# sepal info = first 2 columns, petal = last 2
data = iris.data[:, -2:]  # last 2
# data = iris.data[:, :2]  # first 2
# contains 3 targets: setosa, versicolor, and virginica
target = iris.target
# show the original data
for idx, d in enumerate(data):
    if idx >= 0 and idx <= 49:
        plt.plot(d[0], d[1], 'b.')
    elif idx >= 50 and idx <= 99:
        plt.plot(d[0], d[1], 'y*')
    else:
        plt.plot(d[0], d[1], 'r+')
# plt.scatter(data[:, 0], data[:, 1], c=target, cmap='rainbow')
plt.xlabel(iris.feature_names[2])
plt.ylabel(iris.feature_names[3])
plt.show()

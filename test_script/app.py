# libraries import
import time
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn import datasets
from sklearn.metrics import fowlkes_mallows_score as fms
from sklearn.metrics.cluster import v_measure_score as vms
from scipy.spatial.distance import cdist


# load iris dataset
dataset = datasets.load_iris()

# get all rows of the first two columns of feature for the data and all the existing targets
# data = dataset.data[:, :2]  # sepal
data = dataset.data[:, -2:]  # petal
# data = dataset.data
target = dataset.target
# parameters
verbose = False
tolerance = 1e-4
max_iter = 100

start_time = time.perf_counter()
kmeans_ori = KMeans(n_clusters=3, init="random", n_init=1, max_iter=max_iter, algorithm='full', tol=tolerance,
                    random_state=np.random.RandomState().randint(0, 1000), verbose=verbose)
kmeans_ori.fit(data)
print("Traditional converges at %f" % (time.perf_counter() - start_time))

start_time = time.perf_counter()
kmeans_pp = KMeans(n_clusters=3, init="k-means++", n_init=1, max_iter=max_iter, algorithm='full',
                   tol=tolerance, verbose=verbose)
kmeans_pp.fit(data)
print("K-Means++ converges at %f" % (time.perf_counter() - start_time))

start_time = time.perf_counter()
grand_center = np.mean(data, axis=0)
data_unique = np.unique(data, axis=0)
dist_data = cdist(data_unique, [grand_center], 'euclidean')
closest = np.argsort(dist_data, axis=0)[:3]
for x in range(0, 3):
    data_unique = np.array(np.delete(data_unique, closest, axis=0))
    grand_center = np.mean(data_unique, axis=0)
    dist_data = cdist(data_unique, [grand_center], 'euclidean')
    closest = np.argsort(dist_data, axis=0)[:3]
init_centroids = np.array([data_unique[idx[0]] for idx in closest])

kmeans_scout = KMeans(n_clusters=3, init=init_centroids, n_init=1, max_iter=max_iter,
                      algorithm='full', tol=tolerance, verbose=verbose)
kmeans_scout.fit(data)
print("Scouting converges at %f" % (time.perf_counter() - start_time))

labels_ori = kmeans_ori.labels_
labels_pp = kmeans_pp.labels_
labels_scout = kmeans_scout.labels_
print("\nFowlkes-Mallows Score")
print("Random and Truth")
print(round(fms(target, labels_ori) * 100, 4))
print("K-Means++ and Truth")
print(round(fms(target, labels_pp) * 100, 4))
print("Scout and Truth")
print(round(fms(target, labels_scout) * 100, 4))
print("\nV-Measure Scores")
print("Random and Truth")
print(round(vms(target, labels_ori) * 100, 4))
print("K-Means++ and Truth")
print(round(vms(target, labels_pp) * 100, 4))
print("Scout and Truth")
print(round(vms(target, labels_scout) * 100, 4))

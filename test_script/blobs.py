import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs
from KMeansModel import KMeansModel
from scipy.spatial.distance import cdist
from sklearn.metrics import fowlkes_mallows_score as fms
from sklearn.metrics.cluster import v_measure_score as vms


cluster_no = 5
data = make_blobs(n_samples=200, n_features=2, centers=cluster_no, cluster_std=2.5,
                  random_state=1000, shuffle=True, center_box=(-15, 15))
# print(data)

target = data[1]

verbose = True
tol = 1e-4
max_iter = 100
repeat = 10
grand_center = np.mean(data[0], axis=0)
data_unique = np.unique(data[0], axis=0)
dist_data = cdist(data_unique, [grand_center], 'euclidean')
closest = np.argsort(dist_data, axis=0)[:cluster_no]
# pts = np.array([data_unique[idx[0]] for idx in closest])
# plt.plot(grand_center[0], grand_center[1], 'kx')
# plt.plot(pts[:, 0], pts[:, 1], 'r^')
# plt.scatter(data[0][:, 0], data[0][:, 1], c=target, cmap='viridis')
# plt.show()
for x in range(0, cluster_no):
    # ensure the data is at least 2 times clusters
    if len(data_unique) >= (cluster_no * 2):
        data_unique = np.array(np.delete(data_unique, closest, axis=0))
        grand_center = np.mean(data_unique, axis=0)
        dist_data = cdist(data_unique, [grand_center], 'euclidean')
        closest = np.argsort(dist_data, axis=0)[:cluster_no]
        # pts = np.array([data_unique[idx[0]] for idx in closest])
        # plt.plot(grand_center[0], grand_center[1], 'kx')
        # plt.plot(pts[:, 0], pts[:, 1], 'r^')
        # plt.scatter(data[0][:, 0], data[0][:, 1], c=target, cmap='viridis')
        # plt.show()
init_centroids = np.array([data_unique[idx[0]] for idx in closest])

init = init_centroids
# init = 'k-means++'

model = KMeans(n_clusters=cluster_no, init=init, n_init=1, max_iter=max_iter,
               algorithm='full', tol=tol, verbose=verbose,
               random_state=np.random.RandomState().randint(0, 1000))
model.fit(data[0])
labels = model.labels_
centers = model.cluster_centers_

# print(target)
# print(labels)
print("Fowlkes-Mallows: ", round(vms(target, labels) * 100, 4))
print("V-Measure: ", round(fms(target, labels) * 100, 4))

# plt.plot(grand_center[0], grand_center[1], 'kx')
# plt.scatter(data[0][:, 0], data[0][:, 1], c=labels, cmap='viridis')
# plt.scatter(centers[:, 0], centers[:, 1], c='black', marker='^')
# plt.show()

# libraries import
import time
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import euclidean_distances as ed
from scipy.spatial.distance import cdist


# load iris dataset
iris = datasets.load_iris()

# the iris contains 4 attributes: sepal length, sepal width, petal length, petal width
# get all rows of the 1st two columns of feature for the data and all the existing targets
# data = iris.data[:, -2:]  # petal
data = iris.data[:, :2]  # sepal
# contains 3 targets: setosa, versicolor, and virginica
target = iris.target

start_time = time.perf_counter()
# find the grand center (camp) of the data points
grand_center = np.mean(data, axis=0)
# ensure data is unique
data_unique = np.unique(data, axis=0)
# get unique closest points
# use cdist in SciPy so it is faster
dist_data = cdist(data_unique, [grand_center], 'euclidean')
closest = np.argsort(dist_data, axis=0)[:3]

for x in range(0, 3):
    # remove the closest found
    data_unique = np.array(np.delete(data_unique, closest, axis=0))
    # move the camp based on unique data
    grand_center = np.mean(data_unique, axis=0)
    # recaculate the distance of the data
    dist_data = cdist(data_unique, [grand_center], 'euclidean')
    # find another closest points
    closest = np.argsort(dist_data, axis=0)[:3]

points_closest = np.array([data_unique[idx[0]] for idx in closest])

# current features are 2 (only sepal or only petal), and each feature has 3 different clusters
# customized initial centroids with shape of 3 x 2 (n_clusters, n_features)
# n_init must be 1 because we use initial centroids
model = KMeans(n_clusters=3, init=points_closest, n_init=1, max_iter=100,
               tol=1e-4, algorithm='full', verbose=True)
model.fit(data)
print("Converges in %f seconds" % (time.perf_counter() - start_time))
labels = model.labels_
centers = model.cluster_centers_
# take all rows from the third and fourth column, plot and show using scatterplot with centroids displayed
# plt.xlabel(iris.feature_names[2])
# plt.ylabel(iris.feature_names[3])
plt.plot(grand_center[0], grand_center[1], 'kX')
plt.plot(points_closest[:, 0], points_closest[:, 1], 'g^')
plt.scatter(data[:, 0], data[:, 1], c=labels, cmap='viridis')
plt.scatter(centers[:, 0], centers[:, 1], c='black', marker='+')
plt.scatter(data[:, 0], data[:, 1], c=target, marker='_', cmap='rainbow')
plt.show()

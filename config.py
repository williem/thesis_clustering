verbose = False
tol = 1e-4
max_iter = 100
repeat = 10
rep_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
tol_list = [0.1, 0.01, 0.001, 0.0001]

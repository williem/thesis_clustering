import numpy as np
import pandas as pd
from sklearn import datasets
from sklearn.datasets import make_blobs


class DataModel():
    def __init__(self, dataset_name, option=''):
        self._dataset_name = dataset_name
        self._dataset = None
        self._target = None
        self._option = option

    def _generate_dataset(self):
        if self._dataset_name == 'iris':  # iris dataset
            dataset = datasets.load_iris()
            self._dataset = dataset.data[:, -2:] if self._option == '1' else dataset.data[:, :2]
            self._target = dataset.target
        elif self._dataset_name == 'ecoli':  # e. coli dataset
            dataset = pd.read_csv('data/ecoli.csv', header=None)
            self._dataset = dataset.loc[:, [1, 2, 3, 4, 5, 6, 7]]
            target_dataset = dataset[8].tolist()
            tbl, target = np.unique(target_dataset, return_inverse=True)
            self._target = target
        else:  # synthetic dataset, pass the option as the cluster number
            dataset = make_blobs(n_samples=200, n_features=2, centers=int(self._option), cluster_std=2.5,
                                 random_state=1000, shuffle=False, center_box=(-15, 15))
            self._dataset = dataset[0]
            self._target = dataset[1]

    def get_dataset(self):
        self._generate_dataset()
        return self._dataset, self._target

import os
import sys

import config
from DataModel import DataModel as data_model
from GraphModel import GraphModel as graph_model
from KMeansModel import KMeansModel


def write_to_txt(data_list, d_type, name, labels, option_list):
    filename = 'Repetition' if name == 'rep' else 'Tolerance'
    file = 'results/%s.txt' % filename
    f = open(file, 'a')
    f.write('%s: %s\n' % (d_type, option_list))
    for item, label in zip(data_list, labels):
        f.write('%s\n' % label)
        for i in item:
            f.write('%f ' % i)
        f.write('\n\n')
    f.close()


def show_graph(data, target, config, clusters, option):
    verbose = config.verbose
    tol = config.tol
    max_iter = config.max_iter
    repeat = config.repeat
    opt_list = config.rep_list if option == 'rep' else config.tol_list
    list_convergence = []
    list_fms = []
    list_vms = []
    list_labels = ['K-Means', 'K-Means++', 'Scouting']
    for opt in opt_list:
        trd = KMeansModel('random', data, target, verbose=verbose, cluster=clusters,
                          tol=opt if option != 'rep' else tol, max_iter=max_iter,
                          repeat=opt if option == 'rep' else repeat)
        kpp = KMeansModel('k-means++', data, target, verbose=verbose, cluster=clusters,
                          tol=opt if option != 'rep' else tol, max_iter=max_iter,
                          repeat=opt if option == 'rep' else repeat)
        sct = KMeansModel('scouting', data, target, verbose=verbose, cluster=clusters,
                          tol=opt if option != 'rep' else tol, max_iter=max_iter,
                          repeat=opt if option == 'rep' else repeat)
        trd.process()
        kpp.process()
        sct.process()
        list_convergence.append([trd.get_convergence(), kpp.get_convergence(), sct.get_convergence()])
        list_fms.append([trd.get_fms(), kpp.get_fms(), sct.get_fms()])
        list_vms.append([trd.get_vms(), kpp.get_vms(), sct.get_vms()])
    list_convergence = list(zip(*list_convergence))
    list_fms = list(zip(*list_fms))
    list_vms = list(zip(*list_vms))
    str_label = 'Repetitions' if option == 'rep' else 'Tolerance'
    conv_model = graph_model(len(opt_list), list_convergence, list_labels,
                             'Convergence (%s)' % str_label, 'Time (s)', opt_list)
    fms_model = graph_model(len(opt_list), list_fms, list_labels,
                            'Fowlkes-Mallows (%s)' % str_label, 'Accuracy (%)', opt_list)
    vms_model = graph_model(len(opt_list), list_vms, list_labels,
                            'V-Measure (%s)' % str_label, 'Accuracy (%)', opt_list)
    write_to_txt(list_convergence, 'Convergence', option, list_labels, opt_list)
    write_to_txt(list_fms, 'Fowlkes-Mallows', option, list_labels, opt_list)
    write_to_txt(list_vms, 'V-Measure', option, list_labels, opt_list)
    conv_model.show_plot()
    fms_model.show_plot()
    vms_model.show_plot()


if __name__ == '__main__':
    if len(sys.argv) == 1:
        data_name = 'iris'
        option = '1'
        clusters = 3
    else:
        data_name, option = sys.argv[1], sys.argv[2]
        clusters = 3 if data_name == 'iris' else int(option)
    data, target = data_model(data_name, option=option).get_dataset()

    if len(sys.argv) == 4:  # show graphs, with the variation of rep and tol
        show_graph(data, target, config, clusters, 'rep')
        show_graph(data, target, config, clusters, 'tol')
    else:  # just get the configuration
        trd = KMeansModel('random', data, target, verbose=config.verbose, cluster=clusters,
                          tol=config.tol, max_iter=config.max_iter, repeat=config.repeat)
        kpp = KMeansModel('k-means++', data, target, verbose=config.verbose, cluster=clusters,
                          tol=config.tol, max_iter=config.max_iter, repeat=config.repeat)
        sct = KMeansModel('scouting', data, target, verbose=config.verbose, cluster=clusters,
                          tol=config.tol, max_iter=config.max_iter, repeat=config.repeat)
        trd.process()
        kpp.process()
        sct.process()
        print('Convergence Time')
        trd.show_convergence()
        kpp.show_convergence()
        sct.show_convergence()
        print('\nFowlkes-Mallows Score')
        trd.show_fms()
        kpp.show_fms()
        sct.show_fms()
        print('\nV-Measure Score')
        trd.show_vms()
        kpp.show_vms()
        sct.show_vms()

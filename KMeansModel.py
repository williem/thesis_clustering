import time

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans
from sklearn.metrics import fowlkes_mallows_score as fms
from sklearn.metrics.cluster import v_measure_score as vms


class KMeansModel():
    def __init__(self, init, data, truth, cluster=3, verbose=False, tol=1e-4, max_iter=100, repeat=1):
        self._init = init
        self._verbose = verbose
        self._tol = tol
        self._max_iter = max_iter
        self._repeat = repeat
        self._data = data
        self._truth = truth
        self._mean_fms = 0.0
        self._mean_vms = 0.0
        self._mean_time = 0.0
        self._labels = []
        self._centers = []
        self._clusters = cluster

    def get_method(self):
        method = {
            'random': 'Traditional',
            'k-means++': 'K-Means++',
            'scouting': 'Scouting'
        }
        return method[self._init]

    def process(self):
        mean_time = mean_vms = mean_fms = 0.0
        for x in range(self._repeat):
            start_time = time.perf_counter()
            if self._init in ['random', 'k-means++']:
                # k-means and k-means++ are the same model, different trigger
                model = KMeans(n_clusters=self._clusters, init=self._init,
                               n_init=1, algorithm='full', tol=self._tol,
                               random_state=np.random.RandomState().randint(0, 1000),
                               max_iter=self._max_iter, verbose=self._verbose)
            else:
                grand_center = np.mean(self._data, axis=0)
                data_unique = np.unique(self._data, axis=0)
                dist_data = cdist(data_unique, [grand_center], 'euclidean')
                closest = np.argsort(dist_data, axis=0)[:self._clusters]
                for x in range(0, self._clusters):
                    # ensure the data is at least 2 times clusters
                    if len(data_unique) >= (2 * self._clusters):
                        data_unique = np.array(
                            np.delete(data_unique, closest, axis=0))
                        grand_center = np.mean(data_unique, axis=0)
                        dist_data = cdist(
                            data_unique, [grand_center], 'euclidean')
                        closest = np.argsort(dist_data, axis=0)[
                                             :self._clusters]
                init_centroids = np.array(
                    [data_unique[idx[0]] for idx in closest])

                model = KMeans(n_clusters=self._clusters, init=init_centroids, n_init=1, max_iter=self._max_iter,
                               algorithm='full', tol=self._tol, verbose=self._verbose)

            model.fit(self._data)
            mean_time += time.perf_counter() - start_time
            mean_fms += fms(self._truth, model.labels_) * 100
            mean_vms += vms(self._truth, model.labels_) * 100
        self._labels = model.labels_
        self._centers = model.cluster_centers_
        self._mean_fms = mean_fms / self._repeat
        self._mean_vms = mean_vms / self._repeat
        self._mean_time = mean_time / self._repeat

    def get_convergence(self):
        return self._mean_time

    def get_fms(self):
        return round(self._mean_fms, 4)

    def get_vms(self):
        return round(self._mean_vms, 4)

    def show_convergence(self):
        print("%s converges at %f" % (self.get_method(), self._mean_time))

    def show_fms(self):
        print("%s v.s. Truth : %f" % (self.get_method(), round(self._mean_fms, 4)))

    def show_vms(self):
        print("%s v.s. Truth : %f" % (self.get_method(), round(self._mean_vms, 4)))
